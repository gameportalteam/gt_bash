#!/bin/bash
#
# Delete old container and create a new one for Mongo

# Exit script with error if it is occured
set -e

database_host_name="gametrack-postgres"
docker_image_name="gametrack-node"

# Delete node container
if docker ps | grep -q ${docker_image_name}; then
  echo 'Stop docker container:'
  docker stop ${docker_image_name}
fi

# Delete node images
if docker ps -a | grep -q ${docker_image_name}; then
  echo 'Removing docker container:'
  docker rm ${docker_image_name}
fi

# Install database server
# docker inspect -f {{.State.Running}} gametrack-node

docker run \
  --name gametrack-node \
  --net=gametrack \
  --env "POSTGRES_HOST=${database_host_name}" \
  --env "NODE_ENV=production" \
  -p 80:9000 \
  -d gametrack/gametrack-node-application ./node_modules/.bin/gulp serve:dist

