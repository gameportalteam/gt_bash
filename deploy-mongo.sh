#!/bin/bash
#
# Delete old container and create a new one for Mongo

# Exit script with error if it is occured
set -e

home_user_dir="$HOME"
mongo_db_dir="${home_user_dir}/database/mongo"
docker_image_name="gametrack-mongo"

# Delete mongo container
if docker ps | grep -q ${docker_image_name}; then
  echo 'Stop docker container:'
  docker stop ${docker_image_name}
fi

# Delete mongo images
if docker ps -a | grep -q ${docker_image_name}; then
  echo 'Removing docker container:'
  docker rm ${docker_image_name}
fi

# Create database folder if not exist
mkdir -p "${mongo_db_dir}"

# Install database server
# docker pull mvertes/alpine-mongo
# docker inspect -f {{.State.Running}} gametrack-mongo

docker run \
  --name ${docker_image_name} \
  --net=gametrack \
  -v "${mongo_db_dir}:/data/db" \
  -d mvertes/alpine-mongo

