
#!/bin/bash
#
# Delete old container and create a new one for Mongo

# Exit script with error if it is occured
set -e

home_user_dir="$HOME"
postgres_db_dir="${home_user_dir}/database/postgres"
docker_image_name="gametrack-postgres"

# Delete mongo container
if docker ps | grep -q ${docker_image_name}; then
  echo 'Stop docker container:'
  docker stop ${docker_image_name}
  sleep 2s
fi

# Delete mongo images
if docker ps -a | grep -q ${docker_image_name}; then
  echo 'Removing docker container:'
  docker rm ${docker_image_name}
fi

# Create database folder if not exist
mkdir -p "${postgres_db_dir}"

docker run \
  --name ${docker_image_name} \
  -e "POSTGRES_USER=sid" \
  -e "POSTGRES_PASSWORD=foobar" \
  -e "POSTGRES_DB=test" \
  -e "PGDATA=/var/lib/postgresql/data/pgdata" \
  -v "${postgres_db_dir}:/var/lib/postgresql/data/pgdata" \
  --net=gametrack \
  -d postgres
